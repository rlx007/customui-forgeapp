import Resolver from "@forge/resolver";

const resolver = new Resolver();

resolver.define("setRecording", (req) => {
  console.log(req);
  console.log("content", req.context.extension.content);
  console.log("space", req.context.extension.space);
  return "hello";
});

export const handler = resolver.getDefinitions();
