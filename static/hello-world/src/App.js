import React, { useEffect, useState } from "react";
import { invoke } from "@forge/bridge";
import "./app.css";
import PauseIcon from "@material-ui/icons/Pause";
import StopIcon from "@material-ui/icons/Stop";
import FiberManualRecordIcon from "@material-ui/icons/FiberManualRecord";

const fetchAudio = async () => {
  const response = await api
    .asApp()
    .requestConfluence(
      "https://harshit-tomar.atlassian.net/wiki/download/attachments/557079/matlab%20onramp.pdf?version=1&modificationDate=1632650909179&cacheVersion=1&api=v2",
      {
        headers: {
          Accept: "application/json",
        },
      }
    );

  console.log(`Response: ${response.status} ${response.statusText}`);
  console.log(await response.json());
};

function App() {
  const [isRecording, setisRecording] = useState(false);
  const [startRecording, setstartRecording] = useState(false);
  const [stopRecording, setstopRecording] = useState(false);

  const [rec, setrec] = useState(null);
  const [recStream, setrecStream] = useState(null);
  const [audioURL, setaudioURL] = useState(null);
  const [audioctx, setaudioctx] = useState(new window.AudioContext());

  useEffect(() => {
    invoke("setRecording", { recording: true, audio: audioctx })
      .then((res) => {
        // console.log(res);
        console.log("App Started");
      })
      .catch((err) => console.log(err));
  }, []);

  useEffect(() => {
    if (startRecording) {
      navigator.mediaDevices
        .getUserMedia({
          audio: true,
          video: false,
        })
        .then(function (stream) {
          console.log("somelkjlk");
          console.log(stream);
          setrecStream(stream);
          console.log(audioctx);
          const input = audioctx.createMediaStreamSource(stream);
          console.log(input);
          setrec(
            new window.Recorder(input, {
              numChannel: 1,
            })
          );
          setisRecording(true);
          console.log("recording started");
        })
        .catch((err) => {
          console.log("ERROR", err);
          console.log("some error");
        });
    }
  }, [startRecording]);

  // useEffect(() => {
  //   if (startRecording) {
  //     if (isRecording) {
  //       rec.record();
  //     } else {
  //       rec.stop();
  //     }
  //   }
  // }, [isRecording]);

  // useEffect(() => {
  //   if (startRecording) {
  //     if (stopRecording) {
  //       setisRecording(false);
  //       setstartRecording(false);
  //       rec.stop();
  //       console.log("recStream", recStream);
  //       recStream.getAudioTracks()[0].stop();
  //       rec.exportWAV(createDownloadLink);
  //     }
  //   }
  // }, [stopRecording]);

  // const createDownloadLink = (blob) => {
  //   console.log(blob);
  //   const url = window.URL.createObjectURL(blob);
  //   setaudioURL(url);
  // };

  // useEffect(() => {
  //   console.log("start", startRecording);
  //   console.log("is", isRecording);
  //   console.log("stop", stopRecording);
  // }, [startRecording, isRecording, stopRecording]);

  // const [data] = useState(async () => await fetchCommentsForContent());

  return (
    <div>
      <div className="container">
        <audio controls={true} src="" />
        {!startRecording ? (
          <FiberManualRecordIcon
            onClick={() => {
              setstartRecording(true);
            }}
            className="music-btn"
          />
        ) : isRecording ? (
          <PauseIcon
            className="music-btn"
            onClick={() => setisRecording((prev) => !prev)}
          />
        ) : (
          <FiberManualRecordIcon
            className="music-btn"
            onClick={() => setisRecording((prev) => !prev)}
          />
        )}

        <StopIcon
          onClick={() => setstopRecording(true)}
          className="music-btn"
        />
      </div>
    </div>
  );
}

export default App;
